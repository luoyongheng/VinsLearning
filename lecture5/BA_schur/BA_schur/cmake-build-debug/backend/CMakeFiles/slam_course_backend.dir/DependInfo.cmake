# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/edge.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/edge.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/edge_imu.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/edge_imu.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/edge_prior.cpp" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/edge_prior.cpp.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/edge_reprojection.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/edge_reprojection.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/imu_integration.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/imu_integration.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/loss_function.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/loss_function.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/problem.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/problem.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/vertex.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/vertex.cc.o"
  "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/backend/vertex_pose.cc" "/home/luoyongheng/CLionProjects/VinsLearning/lecture5/BA_schur/BA_schur/cmake-build-debug/backend/CMakeFiles/slam_course_backend.dir/vertex_pose.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "USE_OPENMP"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "/usr/include/opencv"
  "../thirdparty/Sophus"
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
